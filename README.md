Para ilmuwan yakin, tidak akan butuh waktu lama untuk bisa menumbangkan kejayaan internet. Dalam kurun delapan tahun ke depan, internet akan hancur jika dunia tidak mencari solusinya dari sekarang. [konsultan seo](http://www.johanes.my.id/)

Ini merupakan peringatan yang dilayangkan para ilmuwan dari Inggris. Ilmuwan yang tergabung dalam Royal Society itu yakin jika dunia harus melakukan sesuatu untuk meningkatkan infrastruktur komunikasi.

Dilansir melalui News Max, Senin 4 Mei 2015, saat ini layanan internet sangat bergantung pada kabel dan fiber optik. Keduanya digunakan untuk mentransmisikan sinyal ke komputer dan smartphone. Namun sayangnya, keterbatasan internet akan muncul pada 2023.

"Kita sedang dalam tahap mencapai titik puncak. Internet akan mencapai titik batasannya delapan tahun lagi," ujar Profesor Andrew Ellis, salah satu ilmuwan dalam organisasi tersebut.

Menurut Ellis, saat ini kebutuhan manusia akan kecepatan akses data masih bisa ditanggulangi. Namun dalam penelitian di lab, ilmuwan merasa telah 'mentok' dan tidak mendapatkan data lagi dalam penggunaan fiber optik tunggal.

"Intensitasnya sama dengan saat anda berdiri menghadap cahaya matahari. Pengembangannya ke pasar adalah sekitar enam sampai delapan tahun setelah penelitian lab. Jadi dalam kurun delapan tahun ke depan, itulah batasnya. Kita tidak akan bisa mendapatkan lebih banyak data lagi," ujar ilmuwan yang berasal dari Aston University di Birmingham, Inggris itu.

Artinya, kata Ellis, jika infrastruktur yang ada sekarang telah mencapai puncak kapasitas, semua orang di dunia akan menghadapi masalah besar, baik tagihan yang tinggi atau pelayanan yang bermasalah.
Oleh karena itu, Royal Society akan mengumpulkan para ilmuwan, praktisi dan perusahaan telekomunikasi, serta fisikawan untuk mendiskusikan masalah ini. Pertemuan tersebut akan berlangsung bulan ini di Inggris. [https://royal369slot.com/](https://royal369slot.com/)

"Seiring dengan kebutuhan manusia akan internet yang semakin tinggi, akan sulit bagi penyedia untuk memenuhinya di kemudian hari. Sekarang kita masih bisa menanganinya. Tapi suatu saat kita akan kewalahan kecuali jika kita memiliki ide yang radikal. Ke depan, kita akan lihat banyaknya peningkatan biaya secara dramatis yang harus dialami pengguna," ujarnya.

Fiber memang diandalkan untuk mengirimkan informasi dengan cara mengubah sinyal menjadi cahaya dan kembali berubah menjadi sekumpulan informasi. Perusahaan internet telah bertahun-tahun mengandalkan serat tunggal. Ukurannya memang cukup kecil, setipis helai rambut manusia. Belakangan dipercaya jika serat optik mulai kekurangan kapasitas fisik. Berkurangnya kapasitas inilah yang dipercaya dapat memicu membengkaknya biaya internet pengguna.

"Kita siap untuk membayar lebih? Atau seharusnya kita berhenti meningkatkan kapasitas? Internet memiliki kebutuhan energi yang sama dengan industri penerbangan, yakni sekitar dua persen dari konsumsi energi keseluruhan yang dibutuhkan sebuah negara berkembang. Itu hanya untuk transfer data. Jika anda menambahkan komputer, ponsel, televisi, meningkat menjadi delapan persen," ujar Ellis. [alat alat teknik](https://www.karyamakmur.co.id/)
